DELETE FROM users_roles;
DELETE FROM users;
DELETE FROM roles;

INSERT INTO users(id, username, password, first_name, last_name, email, enabled)
VALUES ('ff49ec09-c046-499c-993e-c1637b101f6c', 'william',
        '$2a$10$rGSaRFKwx4NzSlr7/bs4seVQy6a5YKdjlAAfA/Bl47IV0gGFIOITK', 'William', 'Condori', 'william@gmail.com', true);
INSERT INTO users(id, username, password, first_name, last_name, email, enabled)
VALUES ('053429d7-f236-4ecb-bd73-4894dfe55110', 'admin', '$2a$10$rGSaRFKwx4NzSlr7/bs4seVQy6a5YKdjlAAfA/Bl47IV0gGFIOITK',
        'Admin', 'Condori', 'admin@gmail.com', true);

INSERT INTO roles (id, name)
VALUES ('b71ba63c-7c87-403f-ba30-af73c518a687', 'ROLE_USER');
INSERT INTO roles (id, name)
VALUES ('8aafb737-8479-4aa0-88f3-83ff66c3a6c6', 'ROLE_ADMIN');


INSERT INTO users_roles (user_id, roles_id)
VALUES ('ff49ec09-c046-499c-993e-c1637b101f6c', 'b71ba63c-7c87-403f-ba30-af73c518a687');
INSERT INTO users_roles (user_id, roles_id)
VALUES ('053429d7-f236-4ecb-bd73-4894dfe55110', '8aafb737-8479-4aa0-88f3-83ff66c3a6c6');
INSERT INTO users_roles (user_id, roles_id)
VALUES ('053429d7-f236-4ecb-bd73-4894dfe55110', '8aafb737-8479-4aa0-88f3-83ff66c3a6c6');






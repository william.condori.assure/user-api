package com.milankas.training.userapi.web.controller;

import com.milankas.training.userapi.domain.dto.UserResponseDTO;
import com.milankas.training.userapi.domain.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RepositoryRestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/users/healthcheck" , method = RequestMethod.GET)
    public ResponseEntity healthcheck(){
        HashMap<String, String> map = new HashMap<>();
        map.put("status", "Up");
        return ResponseEntity.ok(map);
    }

    @RequestMapping(value = "/users" , method = RequestMethod.GET)
    public ResponseEntity<List<UserResponseDTO>> getAll(){
        return ResponseEntity.ok(userService.getAll());
    }

    @RequestMapping(value = "/users/{userId}" , method = RequestMethod.GET)
    public ResponseEntity<UserResponseDTO> getUser(@PathVariable("userId") String userId){
        return userService.getUser(userId)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
}

package com.milankas.training.userapi.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue
    private UUID id;
    @Column(unique = true, length = 20)
    private String username;
    @Column(length = 60)
    private String password;
    private Boolean enabled;
    @Column(name = "first_name", length = 60)
    private String firstName;
    @Column(name = "last_name", length = 60)
    private String lastName;
    private String email;
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Role> roles;

}

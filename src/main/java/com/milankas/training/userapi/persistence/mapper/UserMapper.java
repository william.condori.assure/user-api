package com.milankas.training.userapi.persistence.mapper;

import com.milankas.training.userapi.domain.dto.UserRequestDTO;
import com.milankas.training.userapi.domain.dto.UserResponseDTO;
import com.milankas.training.userapi.persistence.entity.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface UserMapper {

    UserResponseDTO toUserResponseDTO(User user);

    List<UserResponseDTO> toUserResponseDTOList(List<User> userList);

    User toUser(UserRequestDTO userRequestDTO);

}

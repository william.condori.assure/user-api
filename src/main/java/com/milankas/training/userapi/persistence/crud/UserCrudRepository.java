package com.milankas.training.userapi.persistence.crud;

import com.milankas.training.userapi.persistence.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(path = "users")
public interface UserCrudRepository extends JpaRepository<User, UUID> {

    User findByUsername(String username);

}

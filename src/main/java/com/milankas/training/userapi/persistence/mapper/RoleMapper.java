package com.milankas.training.userapi.persistence.mapper;

import com.milankas.training.userapi.domain.dto.RoleRequestDTO;
import com.milankas.training.userapi.domain.dto.RoleResponseDTO;
import com.milankas.training.userapi.persistence.entity.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    RoleResponseDTO toRoleResponseDTO(Role role);

    Role toRole(RoleRequestDTO roleRequestDTO);

}

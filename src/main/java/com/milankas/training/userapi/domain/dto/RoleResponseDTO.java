package com.milankas.training.userapi.domain.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class RoleResponseDTO {

   private UUID id;
   private String name;

}

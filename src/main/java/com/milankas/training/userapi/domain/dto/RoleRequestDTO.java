package com.milankas.training.userapi.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleRequestDTO {

    private String name;

}

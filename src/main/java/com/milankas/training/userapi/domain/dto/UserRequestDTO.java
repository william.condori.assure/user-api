package com.milankas.training.userapi.domain.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserRequestDTO {

    private String username;
    private String password;
    private Boolean enabled;
    private String firstName;
    private String lastName;
    private String email;
    private List<RoleRequestDTO> roles;

}

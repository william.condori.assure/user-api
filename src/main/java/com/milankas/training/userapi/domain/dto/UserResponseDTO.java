package com.milankas.training.userapi.domain.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class UserResponseDTO {

    private UUID id;
    private String username;
    private Boolean enabled;
    private String firstName;
    private String lastName;
    private String email;
    private List<RoleResponseDTO> roles;

}

package com.milankas.training.userapi.domain.service;

import com.milankas.training.userapi.domain.dto.UserResponseDTO;
import com.milankas.training.userapi.persistence.crud.UserCrudRepository;
import com.milankas.training.userapi.persistence.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    private UserCrudRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    public List<UserResponseDTO> getAll() {
        return userMapper.toUserResponseDTOList(userRepository.findAll());
    }

    public Optional<UserResponseDTO> getUser(String userId) {
        return userRepository.findById(UUID.fromString(userId))
                .map(user -> userMapper.toUserResponseDTO(user));
    }


}
